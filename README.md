# chaotic-earth

simulate nature phenomenons.

### 2022 Hunga Tonga eruption and tsunami

- 2022年1月

### 考察すべき事象

- 1. 火山灰の大気への拡散に依る、地域毎の日射量の影響に関する調査。
- 1. 大気中に舞い上がっただけでなく、海水中にも拡散したかも知れないことから、2つ以上の異なる流体を扱うこととなる。
- 1. 潮位変動に依る津波が発生した理由について。


- GMDH

### 数理モデル

- 粒子やエアロゾル等の拡散・対流モデルを用いる。
- ベクトル解析。
- 回転。

- 行列構造からアルキメデス立体への変換。

- 多様体構造について。


- 噴出量の推定。
- 磁場変動について。

- 水蒸気と水蒸気の雲、灰と灰の雲。
- 降水量カオスと降灰量カオス。



###  データ構造








